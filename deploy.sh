#/bin/sh

if [[ $# -eq 0 ]] ; then
    echo 'Cara pakai : deploy.sh <nama-folder>'
    exit 1
fi

DEPLOY_DIR=$1

rm /var/www/aplikasi-registrasi
ln -s /var/www/${DEPLOY_DIR} /var/www/aplikasi-registrasi
chown -R www-data:www-data /var/www
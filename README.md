# Aplikasi Registrasi Tamu #

1. Jalankan database development

    ```
    docker run --name coba-postgres -e POSTGRES_USER=belajar -e POSTGRES_PASSWORD=docker -e POSTGRES_DB=registrasi-tamu -p 9000:5432 -v /Users/endymuhardin/workspace/training/training-devops-2023-01/registrasi-tamu/db-registrasi:/var/lib/postgresql/data postgres:15-alpine
    ```

2. Menjalankan aplikasi dan database dengan docker compose

    ```
    docker-compose rm -f && docker-compose up --build
    ```

3. Bila project baru saja diclone, maka folder `vendor` biasanya kosong. Untuk mengisinya, kita menjalankan perintah `composer install`. Akan tetapi, biasanya terjadi kesulitan bila versi PHP atau Composer yang terinstal berbeda dengan versi PHP/Composer di komputer programmer aslinya. Solusinya adalah dengan menjalankan perintah `composer install` dalam docker container. Berikut adalah perintahnya

    ```
    docker run -it --rm -v %cd%:/opt -w /opt composer:2 composer install
    ```

    Penjelasan:

    * `-it` : mode interaktif
    * `--rm` : setelah selesai, destroy container
    * `-v %cd%:/opt` : mapping folder saat ini (`%cd%`) ke folder `/opt` dalam container
    * `-w /opt` : jalankan perintah `composer install` di working directory `/opt`
    * `composer:2` : container yang dijalankan menggunakan image `composer` dengan tag `2`
    * 'composer install` : perintah yang dijalankan dalam container